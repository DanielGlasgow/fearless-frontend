window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences';

    try {
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error(`Failed to fetch data from ${url}. Status: ${response.status}`);
    } else {
        const data = await response.json();

        const conference = data.conferences[1];
        const nameTag = document.querySelector('.card-title');
        const descTag = document.querySelector(".card-text");
        nameTag.innerHTML = conference.name;
        // console.log(data)
        // console.log(conference.href)


        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            descTag.innerHTML = details.conference.description;
            // console.log(details.conference.description)
            // console.log('this is details', details);
        }
    }
}   catch (e) {
    console.error(error);
}
})
