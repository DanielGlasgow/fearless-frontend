// import { Fragment } from 'react';
import Nav from './nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        <LocationForm />
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </>
  );
}

export default App;


// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <><Fragment>
//       <Nav />
//     </Fragment><div className="container">
//         <table className="table table-striped">
//           <thead>
//             <tr>
//               <th>Name</th>
//               <th>Conference</th>
//             </tr>
//           </thead>
//           <tbody>
//             {/* for (let attendee of props.attendees) {
//       <tr>
//         <td>{ attendee.name }</td>
//         <td>{ attendee.conference }</td>
//       </tr>
//     } */}
//             {props.attendees?.map(attendee => {
//               return (
//                 <tr key={attendee.href}>
//                   <td>{attendee.name}</td>
//                   <td>{attendee.conference}</td>
//                 </tr>
//               );
//             })}
//           </tbody>
//         </table>
//       </div></>
//   );
// }

// export default App;
